//
//  UIKitExtensions.swift
//  POP-Sample
//
//  Created by Imthath M on 27/10/19.
//  Copyright © 2019 Imthath. All rights reserved.
//

import UIKit



extension UIApplication {
    
    @available(iOS, introduced: 9.0, deprecated: 13.0)
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
    
    func setStatusBarColor(_ color: UIColor?) {
        if #available(iOS 13.0, *) {
            addStatusBarView(withBG: color)
        } else {
             statusBarView?.backgroundColor = color
        }
    }
    
    @available(iOS 13.0, *)
    func addStatusBarView(withBG color: UIColor?) {
        guard let statusBar = keyWindow?.subviews.compactMap({ $0 as? DPStatusBarView }).first else {
            let statusBar = DPStatusBarView(backgroundColor: color)
            keyWindow?.addSubview(statusBar)
            return
        }

        statusBar.backgroundColor = color
    }
    
    @available(iOS 13.0, *)
    func removeStatusBarView() {
        if let statusBar = keyWindow?.subviews.compactMap({ $0 as? DPStatusBarView }).first {
            statusBar.removeFromSuperview()
        }
    }
}

@available(iOS 13, *)
private class DPStatusBarView: UIView {
    
    init(backgroundColor: UIColor?) {
        super.init(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        self.backgroundColor = backgroundColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: 1)
    }
    
    convenience init(rbg: Int) {
        self.init(red: CGFloat(rbg)/255, green: CGFloat(rbg)/255, blue: CGFloat(rbg)/255, alpha: 1)
    }
}
